import 'package:flutter/material.dart';
import 'package:litnet_app/layouts/MainLayout.dart';
import 'package:litnet_app/providers/Authentication.provider.dart';
import 'package:litnet_app/providers/Literature.provider.dart';
import 'package:litnet_app/screens/AddComment.dart';
import 'package:litnet_app/screens/AdsDetail/Detail1.dart';
import 'package:litnet_app/screens/AdsDetail/detail2.dart';
import 'package:litnet_app/screens/AdsDetail/detail3.dart';
import 'package:litnet_app/screens/AdsDetail/detail4.dart';
import 'package:litnet_app/screens/CategoryLiteratures.dart';
import 'package:litnet_app/screens/Home.dart';
import 'package:litnet_app/screens/Latest.dart';
import 'package:litnet_app/screens/LiteratureDetailScreen.dart';
import 'package:litnet_app/screens/Login.dart';
import 'package:litnet_app/screens/UserProfile.dart';
import 'package:litnet_app/screens/splash.dart';
import 'package:litnet_app/screens/userRegister.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: LiteratureProvider()),
        ChangeNotifierProvider.value(value: AuthenticationProvider())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.deepPurple,
          fontFamily:'Poppins'
        ),
        home: SplashScreen(),
        routes:{
          '':(ctx)=>LoginScreen(),
          'home':(ctx)=>MainLayout(),
          'detail':(ctx)=>LiteratureDetail(),
          'category':(ctx)=>CategoryLiterature(),
          'profile-detail':(ctx)=>UserProfile(),
          'register-user':(ctx)=> RegisterScreen(),
          'detail1':(ctx)=> Detail1(),
          'detail2':(ctx)=> Detail2(),
          'detail3':(ctx)=> Detail3(),
          'detail4':(ctx)=> Detail4(),
          'latest':(ctx)=> Latest(),
        }
      ),
    );
  }
}
