import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:litnet_app/screens/Login.dart';

import 'package:litnet_app/utils/theme.dart';



class OnboardinScreen extends StatefulWidget {
  const OnboardinScreen({Key? key}) : super(key: key);

  @override
  State<OnboardinScreen> createState() => _OnboardinScreenState();
}

class _OnboardinScreenState extends State<OnboardinScreen> {

  var selectedColor;
  var colors= [];
  @override
  void initState() {
    // TODO: implement initState
    colors = [
      Colors.blueAccent,
      Colors.amberAccent,
      Colors.lightGreen,
    ];

    selectedColor = colors[0];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: Colors.transparent,
        body: IntroductionScreen(
          isTopSafeArea: true,
          animationDuration: 100,
          globalBackgroundColor: selectedColor,
          controlsPadding: const EdgeInsets.only(bottom: 5),
          done: Text(
            'Continue',
            style: kHeading3,
          ),
          onDone: () {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => LoginScreen()));
          },
          onChange: (index){
            setState(() {
              selectedColor=colors[index];
            });
          },
          next: const Icon(
            Icons.navigate_next_rounded,
            color: Colors.black54,
            size: 35,
          ),
          showSkipButton: true,
          skip: Text(
            'Skip',
            style: kHeading3,
          ),
          dotsDecorator: const DotsDecorator(
              activeColor: Colors.white, color: Colors.black54,),
          pages: [
            PageViewModel(
                decoration: const PageDecoration(
                  pageColor: Colors.blueAccent,
                  imagePadding: EdgeInsets.only(top: 40)
                ),
                image: Image.asset(
                  'assets/images/OBImage2.png',
                  width: 350,
                  fit: BoxFit.contain,
                ),
                titleWidget: Text(
                  'Discover',
                  style: kHeading1,
                ),
                bodyWidget: Text(
                  'LitNet is the app to incourage both readers and writers to explore anywhere and everywhere',
                  style: kHeading2,
                  textAlign: TextAlign.center,
                )),
            PageViewModel(
                decoration: const PageDecoration(
                  pageColor:  Colors.amberAccent,
                  imagePadding: EdgeInsets.only(top: 35)
                ),
                image: Center(
                  child: Image.asset(
                    'assets/images/OBImage1.png',
                    width: 350,
                    fit: BoxFit.contain,
                  ),
                ),
                titleWidget: Text(
                  'Explore with us',
                  style: kHeading1,
                ),
                bodyWidget: Text(
                  'Explore your creativity. Share with us!',
                  style: kHeading2,
                  textAlign: TextAlign.center,
                )),
            PageViewModel(
                decoration: const PageDecoration(
                  imagePadding: EdgeInsets.only(top: 90),
                  pageColor:  Colors.lightGreen,
                ),
                image: Center(
                  child: Image.asset(
                    'assets/images/OBImage3.png',
                    width: 350,
                    fit: BoxFit.contain,
                  ),
                ),
                titleWidget: Text(
                  'Inspire',
                  style: kHeading1,
                ),
                bodyWidget: Text(
                  'Inspire others get Inspired! Share your work in our community. Get recognized',
                  style: kHeading2,
                  textAlign: TextAlign.center,
                )),
          ],
        ));
  }
}
