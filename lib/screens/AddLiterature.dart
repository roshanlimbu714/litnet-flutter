import 'package:flutter/material.dart';
import 'package:litnet_app/models/User.dart';
import 'package:litnet_app/providers/Authentication.provider.dart';
import 'package:litnet_app/providers/Literature.provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddLiterature extends StatefulWidget {
  // int literature;
  // int user;
  //
  // AddLiterature(this.literature, this.user);

  @override
  _AddLiteratureState createState() => _AddLiteratureState();
}

class _AddLiteratureState extends State<AddLiterature> {

  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final _contentForm = GlobalKey<FormState>();
  void loadLiterature() {
    Future.delayed(Duration.zero).then((_) async {
      await Provider.of<LiteratureProvider>(context, listen: false)
          .fetchAndSetLiteratureTypes();
    });
  }

  // Initial Selected Value
  String title = '';
  String content = '';
  String userId = '';
  String dropdownvalue = 'Literature Type';

  Future<void> loadPref()async {
    final SharedPreferences prefs= await _prefs;
    setState(() {
      userId = prefs.getString('userId').toString();
    });
  }

  @override
  void initState(){
    loadPref();
    super.initState();
  }

  Future<void> _addLiterature() async {
    _contentForm.currentState?.save();
    await Provider.of<LiteratureProvider>(context, listen: false)
        .addLiterature(title,
        dropdownvalue,
        content);
    Navigator.of(context).pushNamed('latest');
  }

  @override
  Widget build(BuildContext context) {

    var items = [
      'Literature Type',
    ];

    var litTypes = Provider.of<LiteratureProvider>(context).literatureTypes;

    for(var lit in litTypes){
      items.add(lit.id.toString());
    }

    return Scaffold(
        appBar: AppBar(title: const Text('Post Content')),
        body: SingleChildScrollView(
          child: Container(
              padding: const EdgeInsets.all(8),
              width: double.infinity,
              child: Form(
                key: _contentForm,
                child: SizedBox(
                  height: MediaQuery
                      .of(context)
                      .size
                      .height - 160,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text('Post Content from here', style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold
                      ),),
                      Expanded(
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(top: 16),
                                child: TextFormField(
                                  validator: (val) {
                                    if (val!.isEmpty) {
                                      return 'Title should not be empty';
                                    }
                                    return null;
                                  },
                                  onFieldSubmitted: (value) {
                                  },
                                  onChanged: (val) {
                                    setState(() {
                                      title=val;
                                    });
                                  },
                                  decoration: const InputDecoration(
                                      labelText: 'Title',
                                      prefixIcon: Icon(Icons.title_outlined),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide:
                                        BorderSide(
                                            color: Colors.black87, width: 1.0),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide:
                                        BorderSide(
                                            color: Colors.black54, width: 1),
                                      ),
                                      hintText: 'Enter Title'),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text('Type : ', style: TextStyle(
                                      fontSize: 16
                                    ),),
                                    DropdownButton(
                                      // Initial Value
                                      value: dropdownvalue,
                                      // Down Arrow Icon
                                      icon: const Icon(Icons.keyboard_arrow_down),
                                      // Array list of items
                                      items: items.map((String items) {
                                        return DropdownMenuItem(
                                          value: items,
                                          child: Text(items),
                                        );
                                      }).toList(),
                                      // After selecting the desired option,it will
                                      // change button value to selected value
                                      onChanged: (String? newValue) {
                                        setState(() {
                                          dropdownvalue = newValue!;
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 16),
                                child: TextFormField(
                                  // maxLines: 15,
                                  maxLines: null,
                                  keyboardType: TextInputType.multiline,
                                  validator: (val) {
                                    if (val!.isEmpty) {
                                      return "Content shouldn't be empty";
                                    }
                                    return null;
                                  },
                                  onFieldSubmitted: (value) {
                                  },
                                  onChanged: (val) {
                                    setState(() {
                                      content=val;
                                    });
                                  },
                                  decoration: const InputDecoration(
                                      labelText: 'Content',
                                      prefix: Padding(
                                        padding: EdgeInsets.only(right: 8.0),
                                        child: Icon(Icons.content_copy),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide:
                                        BorderSide(
                                            color: Colors.black87, width: 1.0),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide:
                                        BorderSide(
                                            color: Colors.black54, width: 1),
                                      ),
                                      hintText: 'Enter Content Here'),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: Colors.purple,
                                onPrimary: Colors.white),
                            onPressed: () async {
                              await _addLiterature();
                            },
                            child: const Text('Post' , style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),)),
                      )
                    ],
                  ),
                ),
              )),
        ));
  }
}
