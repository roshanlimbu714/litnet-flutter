import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:litnet_app/models/Literature.model.dart';
import 'package:litnet_app/providers/Authentication.provider.dart';
import 'package:litnet_app/providers/Literature.provider.dart';
import 'package:litnet_app/screens/Advertisement.dart';
import 'package:litnet_app/screens/Login.dart';
import 'package:litnet_app/screens/welcomeCarousal.dart';
import 'package:litnet_app/utils/TopNav.dart';
import 'package:litnet_app/utils/theme.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  void initState() {
    loadLiteratures();
    super.initState();
  }

  void loadLiteratures() {
    Future.delayed(Duration.zero).then((_) async {
      await Provider.of<LiteratureProvider>(context, listen: false)
          .fetchAndSetLiteratures();
    });

    Future.delayed(Duration.zero).then((_) async {
      await Provider.of<LiteratureProvider>(context, listen: false)
          .fetchAndSetLiteratureTypes();
    });
  }

  @override
  Widget build(BuildContext context) {
    var lit = Provider.of<LiteratureProvider>(context).literatures;
    var litTypes = Provider.of<LiteratureProvider>(context).literatureTypes;
    return Scaffold(
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: TopNav(),
      ),
      body: Container(
        padding: const EdgeInsets.all(8),
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text("Discover", style: TextStyle(fontSize: 40, fontWeight: FontWeight.w500),),
                      Text("Literature", style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),)
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset("assets/images/logo2.png", height: 80, width: 80, fit: BoxFit.contain,)
                    ],
                  )
                ],
              ),
              WelcomeCarousel(),
              Container(
                padding: const EdgeInsets.all(8.0),
                child: const Text(
                  'Explore',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    ...litTypes
                        .map((v) => GestureDetector(
                              onTap: () {
                                Navigator.of(context)
                                    .pushNamed('category', arguments: v.title);
                              },
                              child: Container(
                                height: 50,
                                width: 100,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0xFF7B1FA2),
                                          offset: Offset(0, 2.0),
                                          spreadRadius: 1)
                                    ]),
                                margin: const EdgeInsets.all(5.0),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 4.0, horizontal: 8),
                                child: Center(
                                  child: Text(
                                    v.title,
                                    style: kWHeading4,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ),
                            ))
                        .toList()
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Books You May Like',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
                ),
              ),
              Advertisement(),
              const Divider(
                thickness: 2,
                color: Colors.black26,
              ),
              Container(
                padding: const EdgeInsets.only(top: 8, left: 5),
                child: const Text(
                  'Articles',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
                ),
              ),
              ...lit
                  .map(
                    (val) => GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed('detail', arguments: val.id.toString());
                      },
                      child: Center(
                        child: Container(
                          width: MediaQuery.of(context).size.width - 50,
                          height: 200,
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 20),
                          margin: const EdgeInsets.only(top: 15),
                          decoration: kCardDecoration,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                'Title: ' + val.title,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                style: const TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.purple),
                              ),
                              Text(
                                val.content,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                style: const TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                              const Divider(
                                thickness: 1,
                                color: Colors.black12,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(val.user),
                                  Text(DateFormat('MMM dd, yyyy')
                                      .format(DateTime.parse(val.createdAt))
                                      .toString()),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList()
            ],
          ),
        ),
      ),
    );
  }
}
