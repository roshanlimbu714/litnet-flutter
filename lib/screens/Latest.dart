import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:litnet_app/models/Literature.model.dart';
import 'package:litnet_app/providers/Literature.provider.dart';
import 'package:provider/provider.dart';

import '../utils/TopNav.dart';
import '../utils/theme.dart';

class Latest extends StatefulWidget {
  @override
  State<Latest> createState() => _LatestState();
}

class _LatestState extends State<Latest> {
  List<Literature> lit = [];

  @override
  void initState() {
    loadLiteratures();
    super.initState();
  }

  void loadLiteratures() {
    Future.delayed(Duration.zero).then((_) async {
      await Provider.of<LiteratureProvider>(context, listen: false)
          .fetchAndSetLatestLiteratures();
      setState(() {
        lit =
            Provider.of<LiteratureProvider>(context, listen: false).literatures;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'News Feed',
          style: kWHeading4,
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(8),
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              /* const Text(
                'Latest Content',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
              ),*/
              ...lit
                  .map(
                    (val) => GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed('detail', arguments: val.id.toString());
                      },
                      child: Center(
                        child: Container(
                          width: MediaQuery.of(context).size.width - 50,
                          height: 200,
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 20),
                          margin: const EdgeInsets.only(top: 15),
                          decoration: kCardDecoration,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                'Title: ' + val.title,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                style: const TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.purple),
                              ),
                              Text(
                                val.content,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                style: const TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                              const Divider(
                                thickness: 1,
                                color: Colors.black12,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(val.user),
                                  Text(DateFormat('MMM dd, yyyy')
                                      .format(DateTime.parse(val.createdAt))
                                      .toString()),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList()
            ],
          ),
        ),
      ),
    );
  }
}
