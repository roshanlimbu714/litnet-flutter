import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:litnet_app/models/User.dart';
import 'package:litnet_app/providers/Authentication.provider.dart';
import 'package:provider/provider.dart';

import '../utils/theme.dart';

class RegisterScreen extends StatelessWidget {
  final _loginForm = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      // appBar: AppBar(title: const Text('Login')),
      body: Container(
        padding: const EdgeInsets.all(8),
        width: double.infinity,
        child: SingleChildScrollView(
          child: Form(
            key: _loginForm,
            child: Container(
              height: MediaQuery.of(context).size.height+120,
              child: Column(
                children: [
                  Center(
                    child: Column(
                      children: [
                        Container(
                            alignment: Alignment.centerRight,
                            height: 200,
                            width: 200,
                            child: Image.asset('assets/images/logo1.png')),
                        Container(
                          child: const Text(
                            'Please enter your details to continue',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Color.fromRGBO(27, 27, 27, 1),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 16),
                          child: TextFormField(
                            validator: (val) {
                              if (val!.isEmpty) {
                                return 'Username should not be empty';
                              }
                              return null;
                            },
                            onFieldSubmitted: (value) {

                            },
                            onChanged: (val) {

                            },
                            decoration: const InputDecoration(
                                labelText: 'Username',
                                prefixIcon: Icon(Icons.email),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black87, width: 1.0),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black54, width: 1),
                                ),
                                hintText: 'Enter Username'),
                          ),
                        ),Container(
                          margin: const EdgeInsets.only(top: 16),
                          child: TextFormField(
                            validator: (val) {
                              if (val!.isEmpty) {
                                return 'Email should not be empty';
                              }
                              return null;
                            },
                            onFieldSubmitted: (value) {

                            },
                            onChanged: (val) {

                            },
                            decoration: const InputDecoration(
                                labelText: 'Email',
                                prefixIcon: Icon(Icons.email),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black87, width: 1.0),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black54, width: 1),
                                ),
                                hintText: 'Enter Email'),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 16),
                          child: TextFormField(
                            validator: (val) {
                              if (val!.isEmpty) {
                                return 'Password should not be empty';
                              }
                              return null;
                            },
                            onChanged: (value) {

                            },
                            obscureText: true,
                            decoration: const InputDecoration(
                                labelText: 'Password',
                                prefixIcon: Icon(Icons.lock),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black87, width: 1.0),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black54, width: 1),
                                ),
                                hintText: 'Enter Password'),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 16),
                          child: TextFormField(
                            validator: (val) {
                              if (val!.isEmpty) {
                                return 'Password should not be empty';
                              }
                              return null;
                            },
                            onChanged: (value) {

                            },
                            obscureText: true,
                            decoration: const InputDecoration(
                                labelText: 'Password',
                                prefixIcon: Icon(Icons.lock),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black87, width: 1.0),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black54, width: 1),
                                ),
                                hintText: 'Enter Password'),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  Container(
                    width: double.infinity,
                    height: 50,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: Colors.deepPurpleAccent,
                            onPrimary: Colors.white),
                        onPressed: () async {
                          if (_loginForm.currentState!.validate()) {

                          }
                        },
                        child: const Text(
                          'Register',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        )),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Already have an account?',
                        style: kHeading3,
                      ),
                      TextButton(
                        child: Text(
                          'login',
                          style: knormalText,
                        ),
                        onPressed: () {
                          Navigator.of(context).pushNamed('');
                        },
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
