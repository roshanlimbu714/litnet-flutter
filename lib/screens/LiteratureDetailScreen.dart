import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:litnet_app/models/Literature.model.dart';
import 'package:litnet_app/providers/Literature.provider.dart';
import 'package:litnet_app/screens/AddComment.dart';
import 'package:provider/provider.dart';

class LiteratureDetail extends StatefulWidget {
  @override
  State<LiteratureDetail> createState() => _LiteratureDetailState();
}

class _LiteratureDetailState extends State<LiteratureDetail> {
  String litId = '';
  Literature args = Literature(-1, '', '', -1, '', '', []);

  void loadLiterature() {
    Future.delayed(Duration.zero).then((_) async {
      await Provider.of<LiteratureProvider>(context, listen: false)
          .fetchAndSetLiterature(litId);
    });
  }

  @override
  void initState() {
    loadLiterature();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var id = ModalRoute.of(context)?.settings.arguments as String;
    var temp = Provider.of<LiteratureProvider>(context).literature;
    setState(() {
      litId = id;
      args = temp;
    });
    return Scaffold(
      appBar: AppBar(title: Text(args.title)),
      body: Container(
        padding: const EdgeInsets.all(20),
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(args.title,
                  style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.purple)),
              const Divider(
                thickness: 2,
                color: Colors.black26,
              ),
              Text(args.content, style: const TextStyle(fontSize: 20)),
              const Divider(
                thickness: 2,
                color: Colors.black26,
              ),
              Container(
                margin: const EdgeInsets.only(top: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text('Author : ',
                        style: TextStyle(
                            fontSize: 24, color: Colors.deepPurpleAccent)),
                    Row(
                      children: [
                        Text(
                          args.user,
                          style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Comments',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    ...args.comments
                        .map((com) => Container(
                              margin: const EdgeInsets.all(8),
                              padding: const EdgeInsets.all(8),
                              // height: ,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1, color: Colors.black12)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(com.commentedBy, style: const TextStyle(
                                    color: Colors.purple, fontWeight: FontWeight.bold, fontSize: 18
                                  ),),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        width: MediaQuery.of(context).size.width-120,
                                        child: Text(com.comment, style: const TextStyle(
                                          fontSize: 16,
                                        ), maxLines: 8, overflow: TextOverflow.ellipsis,),
                                      ),
                                    ],
                                  ),
                                  Align(
                                    alignment: Alignment.bottomRight,
                                    child: Text(DateFormat('MMM dd, yyyy')
                                        .format(DateTime.parse(com.createdAt))
                                        .toString()),
                                  ),
                                ],
                              ),
                            ))
                        .toList()
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => AddComment(args.id, args.userId)));
        },
        child: const Icon(Icons.add_comment),
      ),
    );
  }
}
