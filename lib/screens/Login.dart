import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:litnet_app/models/User.dart';
import 'package:litnet_app/providers/Authentication.provider.dart';
import 'package:provider/provider.dart';

import '../utils/theme.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  // final emailController = TextEditingController();
  Map<String, String> _authData = {'username': '', 'password': ''};
  final _passwordNode = FocusNode();
  final _loginForm = GlobalKey<FormState>();
  var userDetails = LoginUser('', '');

  Future<void> _loginUser() async {
    _loginForm.currentState!.save();
    await Provider.of<AuthenticationProvider>(context, listen: false)
        .loginUser(userDetails.username, userDetails.password);
    bool isLoggedIn =
        await Provider.of<AuthenticationProvider>(context, listen: false)
            .isLoggedIn;
    if (isLoggedIn) {
      Navigator.of(context).popAndPushNamed('home');
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        backgroundColor: Colors.black12,
        content: Center(
            child: Text(
          "User Logged in",
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
        )),
      ));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Center(
            child: Text(
          "Couldn't login User",
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
        )),
      ));
    }
  }

  @override
  void initState() {
    Provider.of<AuthenticationProvider>(context, listen: false)
        .checkIfAuthenticated();
    bool isLoggedIn =
        Provider.of<AuthenticationProvider>(context, listen: false).isLoggedIn;
    print(isLoggedIn);
    if (isLoggedIn) {
      Navigator.of(context).popAndPushNamed('home');
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(title: const Text('Login')),
      body: Container(
        padding: const EdgeInsets.all(8),
        width: double.infinity,
        child: SingleChildScrollView(
          child: Form(
            key: _loginForm,
            child: Container(
              height: MediaQuery.of(context).size.height - 120,
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                      child: Column(
                        children: [
                          Container(
                              alignment: Alignment.centerRight,
                              height: 200,
                              width: 200,
                              child: Image.asset('assets/images/logo1.png')),
                          Container(
                            child: const Text(
                              'Please enter your details to continue',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(27, 27, 27, 1),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 16),
                            child: TextFormField(
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return 'Username should not be empty';
                                }
                                return null;
                              },
                              onFieldSubmitted: (value) {
                                FocusScope.of(context)
                                    .requestFocus(_passwordNode);
                              },
                              onChanged: (val) {
                                userDetails = LoginUser(
                                    val.toString(), userDetails.password);
                              },
                              decoration: const InputDecoration(
                                  labelText: 'Username',
                                  prefixIcon: Icon(Icons.email),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black87, width: 1.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black54, width: 1),
                                  ),
                                  hintText: 'Enter Username'),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 16),
                            child: TextFormField(
                              focusNode: _passwordNode,
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return 'Password should not be empty';
                                }
                                return null;
                              },
                              onChanged: (value) {
                                // print('asdasd');
                                userDetails.password = value.toString();
                              },
                              obscureText: true,
                              decoration: const InputDecoration(
                                  labelText: 'Password',
                                  prefixIcon: Icon(Icons.lock),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black87, width: 1.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black54, width: 1),
                                  ),
                                  hintText: 'Enter Password'),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 50,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: Colors.deepPurpleAccent,
                            onPrimary: Colors.white),
                        onPressed: () async {
                          if (_loginForm.currentState!.validate()) {
                            await _loginUser();
                          }
                        },
                        child: const Text(
                          'Login',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        )),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Dont have an account?',
                        style: kHeading3,
                      ),
                      TextButton(
                        child: Text(
                          'Register',
                          style: knormalText,
                        ),
                        onPressed: () {
                          Navigator.of(context).pushNamed('register-user');
                        },
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
