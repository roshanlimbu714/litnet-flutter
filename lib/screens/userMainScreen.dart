import 'package:flutter/material.dart';
import 'package:litnet_app/utils/TopNav.dart';
import '../utils/theme.dart';

class UserMainProfile extends StatelessWidget {
  const UserMainProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: TopNav(),
      ),
      body: SingleChildScrollView(
        child: ListView(
          shrinkWrap: true,
          children: [
            Card(
              child: ListTile(
                isThreeLine: true,
                leading: Image.network(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiGcFYBKGruads8sUVAfUBlX8orSdEwuSSTg&usqp=CAU"),
                title: Text(
                  '',
                  style: kHeading2,
                ),
                subtitle: Text(
                  'random-email@email.com',
                  style: kLHeading3,
                ),
                onTap: (){
                  print('profile clicked');
                  Navigator.pushNamed(context, 'profile-detail');
                },
              ),
            ),
            ListTile(
              leading: const Icon(
                Icons.help_outlined,
                color: Colors.green,
              ),
              title: Text(
                'Help and Support',
                style: kLHeading2,
              ),
              onTap: () {},
            ),
            ListTile(
              leading: const Icon(
                Icons.bookmark,
                color: Colors.blueAccent,
              ),
              title: Text(
                'Saved',
                style: kLHeading2,
              ),
              onTap: () {},
            ),
            ListTile(
              leading: const Icon(
                Icons.info_outlined,
                color: Colors.purpleAccent,
              ),
              title: Text(
                'About Us',
                style: kLHeading2,
              ),
              onTap: () {},
            ),
            ListTile(
              leading: const Icon(
                Icons.logout,
                color: Colors.redAccent,
              ),
              title: Text(
                'Logout',
                style: kLHeading2,
              ),
              onTap: () {},
            ),
          ],
        ),
      ),
    );
  }
}