import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:litnet_app/screens/AddLiterature.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../providers/Authentication.provider.dart';
import '../providers/Literature.provider.dart';
import '../utils/TopNav.dart';
import '../utils/theme.dart';
import 'Login.dart';

class UserProfile extends StatefulWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {

  var user;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Future<void> loadPref()async {
    final SharedPreferences prefs= await _prefs;
    setState(() {
      user = prefs.getString('user').toString();
    });
  }

  @override
  void initState() {
    loadLiteratures();
    loadPref();
    super.initState();
  }

  void loadLiteratures() {
    Future.delayed(Duration.zero).then((_) async {
      await Provider.of<LiteratureProvider>(context, listen: false)
          .fetchAndSetLiteratures();
    });
  }

  userLogout(){
    Provider.of<AuthenticationProvider>(context, listen: false)
        .userLogout();
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (BuildContext context) => LoginScreen()));
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      backgroundColor: Colors.black12,
      content: Center(child: Text("User Logged Out", style: TextStyle(
          fontSize: 24,
          fontWeight: FontWeight.bold
      ),)),
    ));
  }

  @override
  Widget build(BuildContext context) {

    var lit = Provider.of<LiteratureProvider>(context).literatures; //fetching literatures

    List userlit = []; //script to extract literatures created by users
    for(var lits in lit){
      if(lits.user.toString() == user){
        userlit.add(lits);
      }else{
        continue;
      }
    }

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        centerTitle: false,
        title: Text(
          'Your Profile',
          style: kWHeading3,
        ),
        iconTheme: const IconThemeData(color: Colors.white),
        elevation: 0,
        actions: [
          IconButton(
            icon: const Icon(Icons.more_vert_rounded),
            onPressed: () {},
          )
        ],
      ),
      body: Container(
        // padding: const EdgeInsets.all(8),
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                height: 300,
                width: double.infinity,
                child: Stack(
                  children: [
                    Container(
                        height: 300,
                        width: double.infinity,
                        child: Image.network(
                          'https://media.istockphoto.com/photos/abstract-texture-gradient-dark-purple-background-with-rough-surface-picture-id1194218198?k=20&m=1194218198&s=612x612&w=0&h=tvPY5T-2AAHjXN70T-xrv4k196rU-C8ZhMVPBo-Jt_8=',
                          fit: BoxFit.cover,
                        )),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Center(
                          child: CircleAvatar(
                            radius: 50.0,
                            backgroundImage: NetworkImage(
                                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiGcFYBKGruads8sUVAfUBlX8orSdEwuSSTg&usqp=CAU"),
                          ),
                        ),
                        Text(
                          '$user',
                          style: kWHeading1,
                        ),
                        Text(
                          'random-email@gmail.com',
                          style: kWHeading2,
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10),
                child: Text(
                  'Your work..',
                  style: kHeading2,
                ),
              ),
              ...userlit
                  .map(
                    (val) => GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .pushNamed('detail', arguments: val.id.toString());
                  },
                  child:  Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width-50,
                      height: 200,
                      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                      margin: const EdgeInsets.only(top: 15),
                      decoration: kCardDecoration,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            'Title: '+val.title,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: const TextStyle(
                                fontSize: 24, fontWeight: FontWeight.w500, color: Colors.purple
                            ),
                          ),
                          Text(
                            val.content,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 3,
                            style: const TextStyle(
                              fontSize: 18,
                            ),
                          ),
                          const Divider(
                            thickness: 1,
                            color: Colors.black12,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(val.user),
                              Text(DateFormat('MMM dd, yyyy')
                                  .format(DateTime.parse(val.createdAt))
                                  .toString()),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ).toList()
            ]
          ),
          
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => AddLiterature()));
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
