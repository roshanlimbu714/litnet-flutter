import 'package:flutter/material.dart';
import 'package:litnet_app/screens/OnboardingScreen.dart';
import 'package:litnet_app/utils/theme.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState(){
    super.initState();
    _navigateToHome();
  }
  
  _navigateToHome() async{
    await Future.delayed(const Duration(seconds: 5), (){});
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> OnboardinScreen()));
  }
  
  @override
  Widget build(BuildContext context) {

    var deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SizedBox(
        height: deviceSize.height,
        width: double.infinity,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                radius:deviceSize.height/5,
                backgroundColor: Colors.white,
                child: Image.network("https://thumbs.dreamstime.com/b/tiny-people-reading-books-digital-library-education-knowledge-studying-literature-concept-cartoon-vector-illustration-style-195319270.jpg", fit: BoxFit.cover,),
              ),
              Text("LitNet", style: kHeading1,),
            ],
          ),
        ),
      ),
    );
  }
}
