import 'package:flutter/material.dart';
import 'package:litnet_app/utils/theme.dart';


class Carousel1  extends StatelessWidget {
  const Carousel1 ({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kCardDecoration,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Image.network('https://cdn.pixabay.com/photo/2016/11/29/07/10/hand-1868015__340.jpg', fit: BoxFit.cover,),
      ),
    );
  }

}