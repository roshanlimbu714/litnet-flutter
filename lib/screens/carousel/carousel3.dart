import 'package:flutter/material.dart';
import 'package:litnet_app/utils/theme.dart';


class Carousel3  extends StatelessWidget {
  const Carousel3 ({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kCardDecoration,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Image.network('https://cdn.pixabay.com/photo/2015/11/07/21/48/iphone-1032779_1280.jpg', fit: BoxFit.cover,),
      ),
    );
  }

}