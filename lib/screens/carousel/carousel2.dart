import 'package:flutter/material.dart';
import 'package:litnet_app/utils/theme.dart';


class Carousel2  extends StatelessWidget {
  const Carousel2 ({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kCardDecoration,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Image.network('https://cdn.pixabay.com/photo/2018/08/14/19/47/reading-3606441_1280.jpg', fit: BoxFit.cover,),
      ),
    );
  }

}