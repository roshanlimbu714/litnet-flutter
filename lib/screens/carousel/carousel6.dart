import 'package:flutter/material.dart';
import 'package:litnet_app/utils/theme.dart';

class Carousel6 extends StatelessWidget {
  const Carousel6({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kCardDecoration,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Container(
          height: 200,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.pink[700],
              boxShadow: const [
                BoxShadow(
                    color: Colors.black54,
                    offset: Offset(0, 2.0),
                    blurRadius: 10,
                    spreadRadius: 2)
              ]),
          child: Container(
            height: 100,
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.red[900],
              boxShadow: const [
                BoxShadow(
                    color: Colors.black54,
                    offset: Offset(0, 2.0),
                    blurRadius: 0,
                    spreadRadius: 2)
              ]),
              child: Container(
                margin: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                'Our attempt is to bring together writers and readers to ecourage each others',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500,color: Colors.white),
              ),
                    ],
                  ))),
        ),
      ),
    );
  }
}
