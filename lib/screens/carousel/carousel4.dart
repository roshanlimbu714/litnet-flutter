import 'package:flutter/material.dart';
import 'package:litnet_app/utils/theme.dart';

class Carousel4 extends StatelessWidget {
  const Carousel4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: kCardDecoration,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Container(
          height: 200,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.amber,
              boxShadow: const [
                BoxShadow(
                    color: Colors.black54,
                    offset: Offset(0, 2.0),
                    blurRadius: 10,
                    spreadRadius: 2)
              ]),
          child: Container(
            height: 100,
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.amber[800],
              boxShadow: const [
                BoxShadow(
                    color: Colors.black54,
                    offset: Offset(0, 2.0),
                    blurRadius: 0,
                    spreadRadius: 2)
              ]),
              child: Container(
                margin: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                'Dear Readers, you read as one would breathe air to fill up and live ',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500,color: Colors.white),
              ),
                    ],
                  ))),
        ),
      ),
    );
  }
}
