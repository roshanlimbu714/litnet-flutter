import 'package:flutter/material.dart';
import 'package:litnet_app/models/User.dart';
import 'package:litnet_app/providers/Authentication.provider.dart';
import 'package:litnet_app/providers/Literature.provider.dart';
import 'package:provider/provider.dart';

class AddComment extends StatefulWidget {
  int literature;
  int user;

  AddComment(this.literature, this.user);

  @override
  _AddCommentState createState() => _AddCommentState();
}

class _AddCommentState extends State<AddComment> {
  // final emailController = TextEditingController();

  final _commentForm = GlobalKey<FormState>();
  String comment = '';

  Future<void> _addComment() async {
    _commentForm.currentState?.save();

    await Provider.of<LiteratureProvider>(context, listen: false)
        .addCommentToLiterature(
      comment,
      widget.literature,
    );
    await Provider.of<LiteratureProvider>(context, listen: false)
        .fetchAndSetLiterature(widget.literature.toString());
    Navigator.of(context).pop();
  }

  void loadLiterature() {
    print(widget.literature);
    Future.delayed(Duration.zero).then((_) async {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Add Comment')),
        body: Container(
            padding: const EdgeInsets.all(8),
            width: double.infinity,
            child: Form(
              key: _commentForm,
              child: SizedBox(
                height: MediaQuery.of(context).size.height - 120,
                child: Column(
                  children: [
                    Expanded(
                      child: Center(
                        child: Column(
                          children: [
                            Container(
                              margin: const EdgeInsets.only(top: 16),
                              child: TextFormField(
                                validator: (val) {
                                  if (val!.isEmpty) {
                                    return 'Comment should not be empty';
                                  }
                                  return null;
                                },
                                onFieldSubmitted: (value) {},
                                onChanged: (val) {
                                  setState(() {
                                    comment = val;
                                  });
                                },
                                decoration: const InputDecoration(
                                    labelText: 'Comment',
                                    prefixIcon: Icon(Icons.add_comment),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black87, width: 1.0),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black54, width: 1),
                                    ),
                                    hintText: 'Enter Comment'),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: double.infinity,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.purple,
                              onPrimary: Colors.white),
                          onPressed: () async {
                            await _addComment();
                          },
                          child: const Text('Submit', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),)),
                    )
                  ],
                ),
              ),
            )));
  }
}
