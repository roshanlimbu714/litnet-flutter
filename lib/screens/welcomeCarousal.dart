import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:litnet_app/screens/carousel/carousel1.dart';
import 'package:litnet_app/screens/carousel/carousel1.dart';
import 'package:litnet_app/screens/carousel/carousel2.dart';
import 'package:litnet_app/screens/carousel/carousel3.dart';
import 'package:litnet_app/screens/carousel/carousel4.dart';
import 'package:litnet_app/screens/carousel/carousel5.dart';
import 'package:litnet_app/screens/carousel/carousel6.dart';

class WelcomeCarousel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      color: Colors.white,
      child: CarouselSlider(
        options: CarouselOptions(
          height: 280.0,

          autoPlay: true,
          //enlargeCenterPage: true,
          autoPlayInterval: Duration(seconds: 3),
        ),
        items: [
          Carousel1(),
          Carousel4(),
          Carousel2(),
          Carousel6(),
          Carousel3(),
          Carousel5(),
        ].map((i) {
          return Builder(
            builder: (BuildContext context) {
              return i;
            },
          );
        }).toList(),
      ),
    );
  }
}
