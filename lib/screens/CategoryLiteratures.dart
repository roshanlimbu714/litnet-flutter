import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:litnet_app/models/Literature.model.dart';
import 'package:litnet_app/providers/Literature.provider.dart';
import 'package:litnet_app/screens/AddComment.dart';
import 'package:litnet_app/utils/theme.dart';
import 'package:provider/provider.dart';

class CategoryLiterature extends StatefulWidget {
  @override
  State<CategoryLiterature> createState() => _CategoryLiteratureState();
}

class _CategoryLiteratureState extends State<CategoryLiterature> {
  String litId = '';
  List<Literature> lits = [];
  Literature args = Literature(-1, '', '', -1, '', '', []);

  void loadLiterature() {
    Future.delayed(Duration.zero).then((_) async {
      await Provider.of<LiteratureProvider>(context, listen: false)
          .fetchAndSetLiteratureByCategory(litId);
    });
    setState(() {
      lits =
          Provider.of<LiteratureProvider>(context, listen: false).literatures;
    });
  }

  @override
  void initState() {
    loadLiterature();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var id = ModalRoute.of(context)?.settings.arguments as String;
    var temp = Provider.of<LiteratureProvider>(context).literature;
    setState(() {
      litId = id;
      args = temp;
    });
    return Scaffold(
      appBar: AppBar(title: Text(id)),
      body: Container(
        padding: EdgeInsets.all(8),
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...lits
                  .map(
                    (val) => GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .pushNamed('detail', arguments: val.id.toString());
                  },
                  child:   Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width-50,
                      height: 200,
                      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                      margin: const EdgeInsets.only(top: 15),
                      decoration: kCardDecoration,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            'Title: '+val.title,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: const TextStyle(
                                fontSize: 24, fontWeight: FontWeight.w500, color: Colors.purple
                            ),
                          ),
                          Text(
                            val.content,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 3,
                            style: const TextStyle(
                              fontSize: 18,
                            ),
                          ),
                          const Divider(
                            thickness: 1,
                            color: Colors.black12,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(val.user),
                              Text(DateFormat('MMM dd, yyyy')
                                  .format(DateTime.parse(val.createdAt))
                                  .toString()),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              )
                  .toList()
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => AddComment(args.id, args.userId)));
        },
        child: Icon(Icons.add_comment),
      ),
    );
  }
}
