import 'dart:ui';
import 'package:flutter/material.dart';

import '../../utils/theme.dart';

class Detail4 extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    var index = ModalRoute.of(context)!.settings.arguments;
    return Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            '',
            style: kLHeading1,
          ),
          iconTheme: const IconThemeData(color: Colors.black54),
          elevation: 0,
          actions: [
            IconButton(
              icon: const Icon(Icons.more_vert_rounded),
              onPressed: () {},
            )
          ],
        ),
        body: Container(
          padding: const EdgeInsets.only(top: 85),
          height: MediaQuery.of(context).size.height,
          width: double.infinity,
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSsf8N7-q8AKvxahFanHJNnJLMRYas7H7MRwQ&usqp=CAU'
                    ),
                fit: BoxFit.cover,
                ),
          ),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4.0, sigmaY: 4.0),
              child: Stack(
                children: [
                  Positioned(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ClipRRect(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(25)),
                          child: Hero(
                            tag: 'add4' ,
                            child: Material(
                              color: Theme.of(context)
                                  .primaryColor
                                  .withOpacity(0.25),
                              child: InkWell(
                                onTap: () =>
                                    {Navigator.of(context).pop(context)},
                                child: Image.network(
                                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSsf8N7-q8AKvxahFanHJNnJLMRYas7H7MRwQ&usqp=CAU',
                                  width: 350,
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                            margin: const EdgeInsets.only(top: 20),
                            padding: const EdgeInsets.all(20),
                            color: Colors.white,
                            width: double.infinity,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  height: 80,
                                  width: 200,
                                  decoration: kCardDecoration1,
                                  child: Center(child: Text('Buy Now',style: kWHeading1,)),
                                ),
                                Text(
                                  '',
                                  style: kHeading1,
                                ),
                                TextButton(onPressed: (){}, child: Text('About the book',style: kHeading1)),
                                Text(
                                  'The short description is exactly that—short and sweet. Consider it a blurb, elevator pitch, or hook. It tells enough about your book to keep readers interested, and delivers enough punch to have them searching for the long book description to read more.',
                                  style: kHeading3,
                                ),
                                Text(
                                  'Consider it a blurb, elevator pitch, or hook. It tells enough about your book to keep readers interested, and delivers enough punch to have them searching for the long book description to read more.',
                                  style: kHeading3,
                                ),
                                Text(
                                  'Consider it a blurb, elevator pitch, or hook. It tells enough about your book to keep readers interested, and delivers enough punch to have them searching for the long book description to read more.',
                                  style: kHeading3,
                                ),
                                Text(
                                  'This is a book for everyone who writes or approves copy: copywriters, multichannel marketers, creative directors, freelance writers, marketing managers . . . even small business owners and information marketers. It reveals dozens of copywriting techniques that can help you write both print and online ads, emails, and websites that are clear, persuasive, and get more attention―and sell more products.',
                                  style: kHeading3,
                                ),
                                Text(
                                  'The short description is exactly that—short and sweet. Consider it a blurb, elevator pitch, or hook. It tells enough about your book to keep readers interested, and delivers enough punch to have them searching for the long book description to read more.',
                                  style: kHeading3,
                                ),
                              ],
                            )),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
