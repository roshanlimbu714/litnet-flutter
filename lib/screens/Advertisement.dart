import 'package:flutter/material.dart';
import 'package:litnet_app/utils/theme.dart';

class Advertisement extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              Stack(
                alignment: AlignmentDirectional.topCenter,
                children: [
                  Container(
                    margin: EdgeInsets.all(8.0),
                    height: 250,
                    width: 200,
                    decoration: kCardDecoration,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.only(left: 8.0, bottom: 15.0),
                          child: Text(
                            'Click to see more details',
                            style: knormalText,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: -5,
                    child: Container(
                      height: 200,
                      width: 150,
                      color: Colors.white,
                      child: Hero(
                        tag: 'add1',
                        child: Material(
                          color:
                              Theme.of(context).primaryColor.withOpacity(0.25),
                          child: InkWell(
                            onTap: () {
                              print('tapped');
                              Navigator.of(context).pushNamed('detail1');
                            },
                            child: Image.network(
                              'https://thumbs.dreamstime.com/z/comic-book-cover-vintage-comics-magazine-layout-cartoon-title-page-vector-template-comic-book-cover-vintage-comics-magazine-layout-131375848.jpg',
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Stack(
                alignment: AlignmentDirectional.topCenter,
                children: [
                  Container(
                    margin: EdgeInsets.all(8.0),
                    height: 250,
                    width: 200,
                    decoration: kCardDecoration,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.only(left: 8.0, bottom: 15.0),
                          child: Text(
                            'Click to see more details',
                            style: knormalText,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                      top: -5,
                      child: Container(
                        height: 200,
                        width: 150,
                        color: Colors.white,
                        child: Hero(
                          tag: 'add2',
                          child: Material(
                            color: Theme.of(context)
                                .primaryColor
                                .withOpacity(0.25),
                            child: InkWell(
                              onTap: () {
                                print('tapped');
                              Navigator.of(context).pushNamed(
                                'detail2',
                              );
                              },
                              child: Image.network(
                                'https://journeyintoeuropedotcom.files.wordpress.com/2015/09/jie-cover-page-001.jpg',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                      )),
                ],
              ),
              Stack(
                alignment: AlignmentDirectional.topCenter,
                children: [
                  Container(
                    margin: EdgeInsets.all(8.0),
                    height: 250,
                    width: 200,
                    decoration: kCardDecoration,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.only(left: 8.0, bottom: 15.0),
                          child: Text(
                            'Click to see more details',
                            style: knormalText,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                      top: -5,
                      child: GestureDetector(
                        onDoubleTap: () {
                          print('tapped');
                          Navigator.of(context).pushNamed(
                            'detail3',
                          );
                        },
                        child: Container(
                          height: 200,
                          width: 150,
                          color: Colors.white,
                          child: Hero(
                            tag: 'add3',
                            child: Material(
                              color: Theme.of(context)
                                  .primaryColor
                                  .withOpacity(0.25),
                              child: InkWell(
                                onTap: () {
                                  print('tapped');
                                Navigator.of(context).pushNamed(
                                  'detail3',
                                );
                                },
                                child: Image.network(
                                  'https://dcassetcdn.com/design_img/3182473/92203/92203_17682310_3182473_99a8f1c1_thumbnail.png',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ),
                      )),
                ],
              ),
              Stack(
                alignment: AlignmentDirectional.topCenter,
                children: [
                  Container(
                    margin: EdgeInsets.all(8.0),
                    height: 250,
                    width: 200,
                    decoration: kCardDecoration,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.only(left: 8.0, bottom: 15.0),
                          child: Text(
                            'Click to see more details',
                            style: knormalText,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                      top: -5,
                      child: Container(
                        height: 200,
                        width: 150,
                        color: Colors.white,
                        child: Hero(
                          tag: 'add4',
                          child: Material(
                            color: Theme.of(context)
                                .primaryColor
                                .withOpacity(0.25),
                            child: InkWell(
                              onTap: () {
                                print('tapped');
                                Navigator.of(context).pushNamed(
                                  'detail4',
                                );
                              },
                              child: Image.network(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSsf8N7-q8AKvxahFanHJNnJLMRYas7H7MRwQ&usqp=CAU',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                      )),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
