import 'dart:ffi';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:litnet_app/models/Literature.model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AuthenticationProvider with ChangeNotifier {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  bool _isLoggedIn = false;
  String _token ='';

  get isLoggedIn{
    return _isLoggedIn;
  }

  Future<void> userLogout() async {
    final SharedPreferences prefs = await _prefs;
    prefs.remove('token');
    prefs.remove('userId');
    _isLoggedIn = false;
    notifyListeners();
  }


  Future<void> loginUser(String username, String password) async {
    final SharedPreferences prefs= await _prefs;
    try {
      var res = await http
          .post(Uri.parse('http://192.168.1.16:3000/auth/login'), body: {
        'username': username,
        'password': password
      });
      var response = json.decode(res.body);
      var userId = response['id'].toString();
      var token = response['accessToken'];
      _isLoggedIn = true;
      await prefs.setString('token', token.toString());
      await prefs.setString('userId', userId);
      await prefs.setString('user', username);
      print(prefs.getString('userId').toString());

      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  Future<void> checkIfAuthenticated() async {
    final SharedPreferences prefs= await _prefs;
    String token = await prefs.getString('token').toString();
    _token =token;
    if(_token != ''){
      _isLoggedIn = true;
    }else{
      _isLoggedIn = false;
    }
  }
}
