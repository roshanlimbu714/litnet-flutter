import 'package:flutter/material.dart';
import 'package:litnet_app/models/Literature.model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class LiteratureProvider with ChangeNotifier {

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  List<Literature> _literatures = [];
  List<LiteratureType> _literatureTypes = [];
  late Literature _literature = Literature(-1, '', '', -1, '', '', []);

  get literatureTypes {
    return _literatureTypes;
  }

  get literatures {
    return _literatures;
  }

  get literature {
    return _literature;
  }

  Future<void> fetchAndSetLatestLiteratures() async {
    try {
      var res = await http
          .get(Uri.parse('http://192.168.1.16:3000/literature/latest'));
      final extractedData = json.decode(res.body);
      List<Literature> litList = [];
      for (var value in extractedData) {
        var extractedDataUser = value['user'];
        var extractedComments = value['comments'];
        List<LiteratureComment> litComments = [];
        for (var comment in extractedComments) {
          litComments.add(
            LiteratureComment(
              comment['comment'],
              comment['commentedBy'],
              comment['createdAt'],
            ),
          );
        }

        litList.add(
          Literature(
              value['id'],
              value['title'],
              value['content'],
              extractedDataUser['id'],
              extractedDataUser['username'],
              value['createdAt'],
              litComments),
        );
      }
      _literatures = litList;
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  Future<void> fetchAndSetLiteratureTypes() async {
    try {
      var res =
          await http.get(Uri.parse('http://192.168.1.16:3000/literature-type'));
      final extractedData = json.decode(res.body);
      List<LiteratureType> litList = [];
      for (var value in extractedData) {
        litList.add(LiteratureType(value['title'], value['id']),);
      }
      _literatureTypes = litList;
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  Future<void> addCommentToLiterature(comment, literature) async {
    final SharedPreferences prefs = await _prefs;
    String id = await prefs.getString('userId').toString();
    print(id);
    var BASE = Uri.parse('http://192.168.1.16:3000/comments/');
    try {
      var res = await http.post(BASE, body: {
        'comment': comment,
        'literature': literature.toString(),
        'user': id
      });
      print(res);
    } catch (e) {
      print(e);
    }
  }

  Future<void> addLiterature(title, type, content) async {
    final SharedPreferences prefs = await _prefs;
    String id = await prefs.getString('userId').toString();
    print(id);
    var BASE = Uri.parse('http://192.168.1.16:3000/literature/');
    try {
      var res = await http.post(BASE, body: {
        'title': title,
        'type': type,
        'user': id,
        'content': content
      });
      print(res);
    } catch (e) {
      print(e);
    }
  }

  Future<void> fetchAndSetLiterature(id) async {
    var BASE = Uri.parse('http://192.168.1.16:3000/literature/' + id);
    try {
      var res = await http.get(BASE);
      final extractedData = json.decode(res.body);
      final extractedDataUser = extractedData['user'];
      final extractedComments = extractedData['comments'];
      List<LiteratureComment> litComments = [];
      for (var comment in extractedComments) {
        litComments.add(
          LiteratureComment(
            comment['comment'],
            comment['commentedBy'],
            comment['createdAt'],
          ),
        );
      }

      _literature = Literature(
          extractedData['id'],
          extractedData['title'],
          extractedData['content'],
          extractedDataUser['id'],
          extractedDataUser['username'],
          extractedData['createdAt'],
          litComments);

      notifyListeners();
    } catch (e) {
      print('error' + e.toString());
    }
    notifyListeners();
  }

  Future<void> fetchAndSetLiteratures() async {
    try {
      var res =
          await http.get(Uri.parse('http://192.168.1.16:3000/literature'));
      final extractedData = json.decode(res.body);
      List<Literature> litList = [];
      for (var value in extractedData) {
        var extractedDataUser = value['user'];
        var extractedComments = value['comments'];
        List<LiteratureComment> litComments = [];
        for (var comment in extractedComments) {
          litComments.add(
            LiteratureComment(
              comment['comment'],
              comment['commentedBy'],
              comment['createdAt'],
            ),
          );
        }

        litList.add(
          Literature(
              value['id'],
              value['title'],
              value['content'],
              extractedDataUser['id'],
              extractedDataUser['username'],
              value['createdAt'],
              litComments),
        );
      }
      _literatures = litList;
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  Future<void> fetchAndSetLiteratureByCategory(id) async {
    var BASE = Uri.parse('http://192.168.1.16:3000/literature-type/name/' + id);
    try {
      var res = await http.get(BASE);
      final extractedData = json.decode(res.body);
      final extractedLiteratures = extractedData['literature'];
      List<Literature> litList = [];

      for (var value in extractedLiteratures) {
        var extractedDataUser = value['user'];
        litList.add(
          Literature(
              value['id'],
              value['title'],
              value['content'],
              extractedDataUser['id'],
              extractedDataUser['username'],
              value['createdAt'], []),
        );
      }
      _literatures = litList;
      notifyListeners();
    } catch (e) {
      print('error' + e.toString());
    }
    notifyListeners();
  }
}
