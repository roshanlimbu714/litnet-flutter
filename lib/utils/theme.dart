
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';

var kHeading1 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.bold,
      color: Color.fromRGBO(27, 27, 27, 1),
    )
);

var kHeading2 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: Color.fromRGBO(27, 27, 27, 1),
    )
);

var kHeading3 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: Color.fromRGBO(27, 27, 27, 1),
    )
);
var kLHeading1 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.bold,
      color: Colors.black54,
    )
);

var kLHeading2 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: Colors.black54,
    )
);

var kLHeading3 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: Colors.black54,
    )
);

var kWHeading1 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.bold,
      color: Colors.white,
    )
);

var kWHeading2 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: Colors.white,
    )
);

var kWHeading4 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.bold,
      color: Colors.white,
    )
);

var kWHeading3 = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: Colors.white,
    )
);

var knormalText = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: Colors.blue,
    )
);

var knormalTextBlack = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 12,
      fontWeight: FontWeight.w300,
      color: Colors.black,
    )
);

var knormalTextPurple = GoogleFonts.poppins(
    textStyle: const TextStyle(
      fontSize: 12,
      fontWeight: FontWeight.w300,
      color: Colors.black,
    )
);

var kCardDecoration = BoxDecoration(
    borderRadius: BorderRadius.circular(10),
    color: Colors.white,
    boxShadow: const [
      BoxShadow(
          color: Colors.black54,
          offset: Offset(0, 2.0),
          blurRadius: 10,
          spreadRadius: 2)
    ]
);
var kCardDecoration1 = BoxDecoration(
    borderRadius: BorderRadius.circular(10),
    color: Colors.green,
    boxShadow: const [
      BoxShadow(
          color: Colors.black26,
          offset: Offset(0, 2.0),
          blurRadius: 10,
          spreadRadius: 2)
    ]
);

var kCardDecoration2 = BoxDecoration(
    borderRadius: BorderRadius.circular(10),
    color: Colors.brown[300],
    boxShadow: const [
      BoxShadow(
          color: Colors.black26,
          offset: Offset(0, 2.0),
          blurRadius: 10,
          spreadRadius: 2)
    ]
);
//color theme
Color ColorLight = Color(0xFFE0F7FA);

Color ColorThemeMain = Colors.cyan;

Color ColorTheme1 = Color(0xFF80DEEA);

Color ColorTheme2 = Color(0xFF84FFFF);

Color ColorThemeCards =  Color(0xFFB2EBF2);

//color theme end


var description = Text('Cows are considered to be a sacred animal in the Hindu religion. The ardent followers of religion worship this animal like a Goddess. A cow has been honored with the status of a mother in Hinduism. This is why people refer to it as ‘Gau Mata’ which translates to Mother Cow.Many followers of religion consider it a sin to kill cows. Nowadays, India has a lot of organizations with the sole purpose of protecting cows. They work to help cows from any danger. They do not tolerate any kind of harm to cows.The government is also taking a lot of measures to protect cows from any injustice. People are coming forward in alliance to safeguard them. They do not prefer any kind of inappropriate behavior with cows. We must work together to protect cows and become the voice for the unspoken.');