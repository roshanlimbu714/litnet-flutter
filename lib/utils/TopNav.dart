import 'package:flutter/material.dart';
import 'package:litnet_app/utils/theme.dart';
import 'package:provider/provider.dart';

import '../providers/Authentication.provider.dart';
import '../screens/Login.dart';

class TopNav extends StatefulWidget {
  const TopNav({Key? key}) : super(key: key);

  @override
  State<TopNav> createState() => _TopNavState();
}

class _TopNavState extends State<TopNav> {

  userLogout(){
    Provider.of<AuthenticationProvider>(context, listen: false)
        .userLogout();
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (BuildContext context) => LoginScreen()));
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      backgroundColor: Colors.black12,
      content: Center(child: Text("User Logged Out", style: TextStyle(
          fontSize: 24,
          fontWeight: FontWeight.bold
      ),)),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
    //backgroundColor: Colors.white,
      title: const Text('Litnet',),
      actions: [
        Ink(
          // color: Colors.white,
            child: IconButton(
                icon: const Icon(Icons.logout_outlined),
                onPressed: () {
                  userLogout();
                }))
      ],
    );
  }
}
