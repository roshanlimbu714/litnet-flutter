import 'package:flutter/material.dart';

class LoginUser{
  String username;
  String password;

  LoginUser(
    @required this.username,
    @required this.password,
  );

  Map<String, dynamic> toJson() => {
    'username': username,
    'password': password,
  };
}

class SignUpUser{
  final String username;
  final String email;
  final String password;

  SignUpUser(
      @required this.username,
      @required this.email,
      @required this.password,
      );
}