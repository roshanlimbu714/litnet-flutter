class Literature {
  final int id;
  final String title;
  final String content;
  final int userId;
  final String user;
  final String createdAt;
  final List<LiteratureComment> comments;

  Literature(this.id, this.title, this.content, this.userId, this.user,
      this.createdAt, this.comments);
}

class LiteratureComment {
  final String comment;
  final String commentedBy;
  final String createdAt;

  LiteratureComment(
    this.comment,
    this.commentedBy,
    this.createdAt,
  );
}

class LiteratureType {
  final String title;
  final int id;

  LiteratureType(this.title, this.id);
}
