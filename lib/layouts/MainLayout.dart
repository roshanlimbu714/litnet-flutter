import 'package:flutter/material.dart';
import 'package:litnet_app/screens/AddLiterature.dart';
import 'package:litnet_app/screens/Latest.dart';
import 'package:litnet_app/screens/Home.dart';
import 'package:litnet_app/screens/UserProfile.dart';
import 'package:litnet_app/screens/userMainScreen.dart';

class MainLayout extends StatefulWidget {
  @override
  _MainLayoutState createState() => _MainLayoutState();
}

class _MainLayoutState extends State<MainLayout> {
  int currentIndex = 0;

  final List<Widget> _pages = [
    Home(),
    Latest(),
    AddLiterature(),
    const UserMainProfile(),
  ];

  @override
  Widget build(BuildContext context) {
    var currIndex = 1;

    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: _pages[currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: true,
          unselectedItemColor: const Color.fromRGBO(255, 255, 255, 0.30),
          backgroundColor: Colors.deepPurpleAccent,
          showUnselectedLabels: true,
          elevation: 3,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home_outlined,
                size: 30,
              ),
              label: 'Home',
              backgroundColor: Colors.deepPurpleAccent,
              activeIcon: Icon(
                Icons.home_rounded,
                size: 30,
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.newspaper_outlined,size: 30),
              label: 'News Feed',
              backgroundColor: Colors.deepPurpleAccent,
              activeIcon: Icon(Icons.newspaper_rounded,size: 30),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.edit_outlined,size: 30),
              label: 'AddContent',
              backgroundColor: Colors.deepPurpleAccent,
              activeIcon: Icon(Icons.edit_rounded,size: 30),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person_outlined,size: 30),
              label: 'Profile',
              backgroundColor: Colors.deepPurpleAccent,
              activeIcon: Icon(Icons.person_rounded,size: 30),
            ),
          ],
          currentIndex: currentIndex,
          selectedItemColor: Colors.white,
          onTap: (index) {
            // _changeRoute(index,context);
            setState(() {
              currentIndex = index;
            });
          },
        ));
  }
}
